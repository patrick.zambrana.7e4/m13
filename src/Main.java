package src;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Adiós mundo, \uD83D\uDC80");
        System.out.println("\uD83D\uDE00 \uD83D\uDD2A");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Te gusta el chocholate?\n" +
                "-Si\n" +
                "-No");
        String respuesta = scanner.next();
        System.out.println(((respuesta.equals("Si")) ? "Muy Bien" : "Muy mal"));
    }
}
