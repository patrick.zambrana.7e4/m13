# Lorem Ipsum

---

1. What is Lorem Ipsum?
2. Where does it come from?
3. Why do we use it?
4. Where can I get some?

Lorem Ipsum is simply dummy text of the printing and typesetting industry.
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
when an unknown printer took a galley of type and scrambled it to make a type
specimen book.

![](lorem_ipsum.png)

- What is Lorem Ipsum? 
- Where does it come from? 
- Why do we use it? 
- Where can I get some?

Info: [Lorem Ipsum page](https://www.lipsum.com/)
